package br.com.treinamento.desafio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ricardo Gelschleiter on 23/11/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Image {

    /**
     * The directory path of to the image.
     */
    private String path;

    /**
     * The file extension for the image.
     */
    private String extension;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
