package br.com.treinamento.desafio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 24/11/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComicDataContainer {

    /**
     * The total number of results returned by this call.
     */
    private int count;

    /**
     * The list of comics returned by the call
     */
    private List<Comic> results;

    public int getCount() {
	return count;
    }

    public void setCount(int count) {
	this.count = count;
    }

    public List<Comic> getResults() {
	return results;
    }

    public void setResults(List<Comic> results) {
	this.results = results;
    }
}
