package br.com.treinamento.desafio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ricardo Gelschleiter on 23/11/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Character {

    /**
     * The unique ID of the character resource.
     */
    private int id;

    /**
     * The name of the character.
     */
    private String name;

    /**
     * A short bio or description of the character.
     */
    private String description;

    /**
     * The representative image for this character.
     */
    private Image thumbnail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Image getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Image thumbnail) {
        this.thumbnail = thumbnail;
    }
}
