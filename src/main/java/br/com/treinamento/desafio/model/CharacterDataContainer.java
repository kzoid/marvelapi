package br.com.treinamento.desafio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 23/11/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CharacterDataContainer {

    /**
     * The total number of results returned by this call.
     */
    private int count;

    /**
     * The list of characters returned by the call.
     */
    private List<Character> results;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Character> getResults() {
        return results;
    }

    public void setResults(List<Character> results) {
        this.results = results;
    }
}
