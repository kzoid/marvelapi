package br.com.treinamento.desafio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ricardo Gelschleiter on 24/11/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Comic {

    /**
     * The unique ID of the comic resource.
     */
    private int id;

    /**
     * The canonical title of the comic.
     */
    private String title;

    /**
     * The preferred description of the comic.
     */
    private String description;

    /**
     * The representative image for this comic.
     */
    private Image thumbnail;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Image getThumbnail() {
	return thumbnail;
    }

    public void setThumbnail(Image thumbnail) {
	this.thumbnail = thumbnail;
    }
}
