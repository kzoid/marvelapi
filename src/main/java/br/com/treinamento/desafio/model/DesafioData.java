package br.com.treinamento.desafio.model;

/**
 * Created by Ricardo Gelschleiter on 24/11/16.
 */
public class DesafioData {

    /**
     * The character resource
     */
    private Character character;

    /**
     * The comic resource
     */
    private ComicDataContainer comics;

    /**
     * The serie resource
     */
    private SeriesDataContainer series;

    public Character getCharacter() {
	return character;
    }

    public void setCharacter(Character character) {
	this.character = character;
    }

    public ComicDataContainer getComics() {
	return comics;
    }

    public void setComics(ComicDataContainer comics) {
	this.comics = comics;
    }

    public SeriesDataContainer getSeries() {
	return series;
    }

    public void setSeries(SeriesDataContainer series) {
	this.series = series;
    }
}
