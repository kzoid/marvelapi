package br.com.treinamento.desafio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ricardo Gelschleiter on 23/11/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CharacterDataWrapper {

    /**
     * The HTTP status code of the returned result.
     */
    private int code;

    /**
     * A string description of the call status.
     */
    private String status;

    /**
     * The results returned by the call.
     */
    private CharacterDataContainer data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CharacterDataContainer getData() {
        return data;
    }

    public void setData(CharacterDataContainer data) {
        this.data = data;
    }
}
