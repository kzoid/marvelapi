package br.com.treinamento.desafio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ricardo Gelschleiter on 24/11/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Series {

    /**
     * The unique ID of the series resource.
     */
    private int id;

    /**
     * The canonical title of the series.
     */
    private String title;

    /**
     * A description of the series.
     */
    private String description;

    /**
     * The first year of publication for the series.
     */
    private int startYear;

    /**
     * The age-appropriateness rating for the series.
     */
    private String rating;

    /**
     * The representative image for this series.
     */
    private Image thumbnail;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public int getStartYear() {
	return startYear;
    }

    public void setStartYear(int startYear) {
	this.startYear = startYear;
    }

    public String getRating() {
	return rating;
    }

    public void setRating(String rating) {
	this.rating = rating;
    }

    public Image getThumbnail() {
	return thumbnail;
    }

    public void setThumbnail(Image thumbnail) {
	this.thumbnail = thumbnail;
    }
}
