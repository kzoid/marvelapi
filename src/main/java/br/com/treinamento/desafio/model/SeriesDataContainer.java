package br.com.treinamento.desafio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Ricardo Gelschleiter on 24/11/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SeriesDataContainer {

    /**
     * The total number of results returned by this call.
     */
    private int count;

    /**
     * The list of series returned by the call
     */
    private List<Series> results;

    public int getCount() {
	return count;
    }

    public void setCount(int count) {
	this.count = count;
    }

    public List<Series> getResults() {
	return results;
    }

    public void setResults(List<Series> results) {
	this.results = results;
    }
}
