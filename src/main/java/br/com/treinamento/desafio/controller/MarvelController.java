package br.com.treinamento.desafio.controller;

import br.com.treinamento.desafio.model.Character;
import br.com.treinamento.desafio.model.CharacterDataWrapper;
import br.com.treinamento.desafio.model.DesafioData;
import br.com.treinamento.desafio.service.MarvelAPIService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Ricardo Gelschleiter on 23/11/2016.
 */
@RestController
public class MarvelController {

    @Autowired
    private MarvelAPIService marvelService;

    @RequestMapping(value = "/character", method = RequestMethod.GET)
    public ResponseEntity<Character> getCharacter(@RequestParam(value = "name") String name) throws Exception {

        String charactersResponse = marvelService.getCharacterTest(name);
        CharacterDataWrapper charactersResult = new Gson().fromJson(charactersResponse, CharacterDataWrapper.class);

        return new ResponseEntity<Character>(charactersResult.getData().getResults().get(0), HttpStatus.OK);
    }

    @RequestMapping(value = "/characterData", method = RequestMethod.GET)
    public ResponseEntity<DesafioData> getCharacterComicsAndSeries(@RequestParam(value = "name") String name) throws Exception {

        return new ResponseEntity<DesafioData>(marvelService.getCharacterComicsAndSeries(name), HttpStatus.OK);
    }

}
