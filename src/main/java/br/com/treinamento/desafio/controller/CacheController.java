package br.com.treinamento.desafio.controller;

import br.com.treinamento.desafio.service.CacheService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

/**
 * Created by Ricardo Gelschleiter on 24/11/2016.
 */
@RestController
@RequestMapping(value = "/data")
public class CacheController {

    @Autowired
    CacheService cacheService;

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> listCacheData() throws Exception {
        Object data = cacheService.getCacheData();

        return new ResponseEntity<String>(new Gson().toJson(data), HttpStatus.OK);
    }

    @RequestMapping(value = "/{charName}", method = RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable("charName") String charName) {
        try {
            cacheService.deleteData(charName);

            return new ResponseEntity<String>("xuxexo", HttpStatus.OK);
        } catch (NoSuchElementException ne) {
            return new ResponseEntity<String>("", HttpStatus.NOT_FOUND);
        }
    }
}
