package br.com.treinamento.desafio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Created by Ricardo Gelschleiter on 24/11/2016.
 */
@Component
public class CacheService {

    @Autowired
    private CacheManager cacheManager;

    /**
     * List all cached data so far
     *
     * @return
     */
    public Object getCacheData() {
        Cache cache = cacheManager.getCache("charData");

        return cache.getNativeCache();
    }

    /**
     * Deletes an specific character data
     *
     * @param charName The character name
     */
    public void deleteData(String charName) {
        Cache cache = cacheManager.getCache("charData");

        if (Objects.isNull(cache.get(charName)))
            throw new NoSuchElementException();

        cache.evict(charName);
    }
}
