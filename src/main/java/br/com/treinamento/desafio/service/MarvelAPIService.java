package br.com.treinamento.desafio.service;

import br.com.treinamento.desafio.model.Character;
import br.com.treinamento.desafio.model.*;
import br.com.treinamento.desafio.service.sender.MarvelAPIRequestSender;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Objects;

/**
 * Created by Ricardo Gelschleiter on 23/11/2016.
 */
@Component
public class MarvelAPIService {

    private static final String SERVICE_URL = "http://gateway.marvel.com:80/v1/public/";
    private static final String CHARACTER_MAP_PATH = "characters";
    private static final String COMICS_MAP_PATH = "comics";
    private static final String SERIES_MAP_PATH = "series";

    @Autowired
    private MarvelAPIRequestSender marvelApi;

    public String getCharacterTest(String charName) throws Exception {

        if (Objects.isNull(charName)) {
            throw new Exception("Missing character name.");
        }

        return getCharacter(charName);
    }

    @Cacheable(cacheNames="charData", key="#charName")
    public DesafioData getCharacterComicsAndSeries(String charName) throws Exception {

        if (Objects.isNull(charName)) {
            throw new Exception("Missing character name.");
        }

        DesafioData data = new DesafioData();

        try {
            ObjectMapper mapper = new ObjectMapper();
            CharacterDataWrapper charactersResult = mapper.readValue(this.getCharacter(charName), CharacterDataWrapper.class);

            if ((HttpStatus.OK.value() == charactersResult.getCode()) && charactersResult.getData().getCount() > 0) {
                // only matter the first one (not really)
                Character character = charactersResult.getData().getResults().get(0);
                data.setCharacter(character);

                JsonParser parser = new JsonParser();

                // find character's comics
                JsonElement comicResultElement = parser.parse(getComicsByCharacterId(character.getId()));
                JsonObject comicDataWrapper = comicResultElement.getAsJsonObject();

                ComicDataContainer comicsData = new Gson().fromJson(parser.parse(comicDataWrapper.get("data").toString()), ComicDataContainer.class);
                data.setComics(comicsData);

                // find character's series
                JsonElement seriesResultElement = parser.parse(getSeriesByCharacterId(character.getId()));
                JsonObject seriesDataWrapper = comicResultElement.getAsJsonObject();

                SeriesDataContainer seriesData = new Gson().fromJson(parser.parse(seriesDataWrapper.get("data").toString()), SeriesDataContainer.class);
                data.setSeries(seriesData);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return data;
    }

    /**
     * Fetches lists of comic characters by name
     *
     * @param name character name
     * @return CharacterDataWrapper
     * @throws UnsupportedEncodingException
     */
    private String getCharacter(String name) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(SERVICE_URL)
                .append(CHARACTER_MAP_PATH)
                .append("?name=")
                .append(URLEncoder.encode(name, "UTF-8"));

        return marvelApi.sendRequest(url.toString());
    }

    /**
     * Fetches lists of comics filtered by a character id limited to lastests 10
     *
     * @param charId The character id.
     * @return ComicDataWrapper JSON
     * @throws Exception
     */
    private String getComicsByCharacterId(int charId) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(SERVICE_URL)
                .append(CHARACTER_MAP_PATH)
                .append("/" + charId + "/")
                .append(COMICS_MAP_PATH)
                .append("?orderBy=-onsaleDate")
                .append("&limit=10");

        return marvelApi.sendRequest(url.toString());
    }

    /**
     * Fetches lists of series filtered by a character id limited to lastests 10
     *
     * @param charId The character id.
     * @return SeriesDataWrapper JSON
     * @throws Exception
     */
    private String getSeriesByCharacterId(int charId) throws Exception {
        StringBuilder url = new StringBuilder();
        url.append(SERVICE_URL)
                .append(CHARACTER_MAP_PATH)
                .append("/" + charId + "/")
                .append(SERIES_MAP_PATH)
                .append("?orderBy=-startYear")
                .append("&limit=10");

        return marvelApi.sendRequest(url.toString());
    }
}
