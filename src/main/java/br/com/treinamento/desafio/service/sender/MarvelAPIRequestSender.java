package br.com.treinamento.desafio.service.sender;

import br.com.treinamento.config.MarvelAPIConfig;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Ricardo Gelschleiter on 23/11/2016.
 * DOC @ https://developer.marvel.com/documentation/authorization
 */
@Component
public class MarvelAPIRequestSender {

    /**
     * Encapsulates the authorization parameters in the request
     *
     * @param url Target API URL
     * @return
     */
    public String sendRequest(String url) throws Exception {
        try {
            Timestamp ts = new Timestamp(new Date().getTime());
            Client client = ClientBuilder.newClient(new ClientConfig());

            /*
                Request example:

                Request Url: http://gateway.marvel.com/v1/public/comics
                Request Method: GET
                Params: {
                  "apikey": "your api key",
                  "ts": "a timestamp",
                  "hash": "your hash"
                }
                Headers: {
                  Accept:
                }
             */
            String response = client.target(url)
                    .queryParam("apikey", MarvelAPIConfig.PUBLIC_KEY)
                    .queryParam("ts", ts)
                    .queryParam("hash", generateHash(ts))
                    .request("application/json").get(String.class);

            return response;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * @param ts a timestamp (or other long string which can change on a request-by-request basis)
     * @return hash - a md5 digest of the ts parameter, your private key and your public key (e.g. md5(ts+privateKey+publicKey)
     * @throws Exception
     */
    private String generateHash(Timestamp ts) throws Exception {
        String hash = ts + MarvelAPIConfig.PRIVATE_KEY + MarvelAPIConfig.PUBLIC_KEY;

        return DigestUtils.md5DigestAsHex(hash.getBytes()).toString();
    }
}
