package br.com.treinamento.service;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.desafio.model.DesafioData;
import br.com.treinamento.desafio.service.CacheService;
import br.com.treinamento.desafio.service.MarvelAPIService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Ricardo Gelschleiter on 25/11/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfig.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CacheServiceTest {

    public final String charName = "thor";

    @Autowired
    private MarvelAPIService marvelAPIService;

    @Autowired
    private CacheService cacheService;

    @Before
    public void initialize() throws Exception {
        marvelAPIService.getCharacterComicsAndSeries(charName);
    }

    @Test
    public void deveRetornarDadosCache() throws Exception {
        ConcurrentHashMap<String, DesafioData> cacheData = (ConcurrentHashMap<String, DesafioData>) cacheService.getCacheData();

        Assert.assertFalse(cacheData.isEmpty());
    }

    @Test
    public void deveRemoverDadosCache() {
        cacheService.deleteData(charName);

        ConcurrentHashMap<String, DesafioData> cacheData = (ConcurrentHashMap<String, DesafioData>) cacheService.getCacheData();

        Assert.assertTrue(cacheData.isEmpty());
    }
}
