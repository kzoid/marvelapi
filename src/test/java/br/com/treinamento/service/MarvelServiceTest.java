package br.com.treinamento.service;

import br.com.treinamento.config.AppConfig;
import br.com.treinamento.desafio.model.DesafioData;
import br.com.treinamento.desafio.service.MarvelAPIService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Objects;

/**
 * Created by Ricardo Gelschleiter on 25/11/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfig.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class MarvelServiceTest {

    @Autowired
    private MarvelAPIService marvelService;

    @Test
    public void deveBuscarDeveBuscarPersonagemConsolidado() throws Exception {
        DesafioData desafioData = marvelService.getCharacterComicsAndSeries("deadpool");

        Assert.assertTrue(Objects.nonNull(desafioData.getCharacter()));
        Assert.assertEquals(desafioData.getCharacter().getName(), "Deadpool");
        Assert.assertTrue(Objects.nonNull(desafioData.getComics()));
        Assert.assertTrue(Objects.nonNull(desafioData.getComics().getCount() > 0));
        Assert.assertTrue(Objects.nonNull(desafioData.getSeries()));
        Assert.assertTrue(Objects.nonNull(desafioData.getSeries().getCount() > 0));
    }

    @Test(expected = Exception.class)
    public void deveGerarExceptionNomeCharacter() throws Exception {
        marvelService.getCharacterComicsAndSeries(null);
    }
}
